package main

import (
	"tdd-rest-api/internal/handler"

	"github.com/gofiber/fiber/v2"
)

func main() {
	a := fiber.New()

	a.Post("api/users", handler.Create)
	a.Get("api/users/:id", handler.Get)
	a.Put("api/users/:id", handler.Update)
	a.Delete("api/users/:id", handler.Delete)

	a.Listen(":3000")
}