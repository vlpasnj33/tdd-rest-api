package handler

import (
	"net/http"

	"tdd-rest-api/internal/models"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
)

func Create(c *fiber.Ctx) error {
	var db *sqlx.DB
	input := models.User{}
	err := c.BodyParser(&input) 
	if err != nil {
		return c.Status(http.StatusBadRequest).SendString("Error in creating request")
	}
	
	_, err = db.Exec("INSERT INTO users (name, email) VALUES ($1, $2)", input.Name, input.Email)
	if err != nil {
		return c.Status(http.StatusInternalServerError).SendString("Internal Server Error")
	}

	return c.JSON(fiber.Map{})
}

func Get(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"status": http.StatusOK})
}

func Update(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"status": http.StatusOK})
}

func Delete(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"status": http.StatusOK})
}
