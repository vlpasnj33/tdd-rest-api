package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"tdd-rest-api/internal/db"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var testDb *sqlx.DB
var err error

func TestInit(t *testing.T) {
	testDb, err = db.TestDB()
	if err != nil {
		t.Errorf("Error in initializing test DB: %v", err)
	}
	fmt.Println(testDb)
}

func TestCreateUserBadRequest(t *testing.T) {
	DropUsersTable(testDb)
	CreateUsersTable(testDb)

	a := fiber.New()
	a.Post("/api/users", Create)

	req, err := http.NewRequest("POST", "/api/users", bytes.NewBuffer([]byte(`{"email": 123}`)))
	if err != nil {
		t.Errorf("Error in creating request: %v", err)
    }

	req.Header.Set("Content-Type", "application/json")

	resp, err := a.Test(req)
	if err != nil {
		t.Errorf("Error in making request: %v", err)
	}
	defer resp.Body.Close()

	assert.EqualValues(t, http.StatusBadRequest, resp.StatusCode)
}

func TestCreateUserSuccess(t *testing.T) {
	DropUsersTable(testDb)
	CreateUsersTable(testDb)

	a := fiber.New()
	a.Post("/api/users", Create)
	req, err := http.NewRequest("POST", "/api/users", bytes.NewBuffer([]byte(`{"name":"John Doe","email":"john@example.com"}`)))
	if err != nil {
		t.Errorf("Error in creating request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := a.Test(req)
	if err != nil {
		t.Errorf("Error in making request: %v", err)
	}
	defer resp.Body.Close()

	assert.EqualValues(t, http.StatusOK, resp.StatusCode)
}

func CreateUsersTable(Db *sqlx.DB) {
	Db.MustExec(`CREATE TABLE IF NOT EXISTS users (
		id SERIAL PRIMARY KEY,
		name VARCHAR(195),
		email VARCHAR(195)
	  );`)
}

func InsertIntoUsersTable(Db *sqlx.DB) {
	Db.MustExec("INSERT INTO users (name, email) VALUES ($1, $2);", "John Doe", "john@example.com")
}

func DropUsersTable(Db *sqlx.DB) {
	Db.MustExec(`DROP TABLE IF EXISTS users;`)
}

func GotData(w *httptest.ResponseRecorder, t *testing.T) map[string]interface{} {
	var got map[string]interface{}
  
	if len(w.Body.Bytes()) != 0 {
	  err := json.Unmarshal(w.Body.Bytes(), &got)
	  if err != nil {
		t.Fatal(err)
	  }
	}
  
	return got
}
  
  
  
