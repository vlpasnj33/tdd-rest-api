package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func TestDB() (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", "user=postgres password=vova1697 dbname=users sslmode=disable")
	return db, err
}